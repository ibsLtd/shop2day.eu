import React from 'react'
import Link from 'next/link'
import {ENV} from '../env'

const Header = props => {


    return (
        <header>
            <div className={'container'}>
                <div className={'row justify-content-between  align-items-center'}>
                    <div className={'col-4  col-md-9 logo-container'}>
                        <Link href={{ pathname: '/', query: { lang: props.lang } }} as={`/${props.lang}`}>
                            <a><img src={`/assets/logo.png`}alt={'iBS | Informatics Business Services'} /></a>
                        </Link>
                    </div>
                    <div className={'col-8  col-md-3 languages-container'}>
                        <ul className={'d-flex justify-content-between align-items-center'}>
                            <li>
                                <Link href={{ pathname: props.href, query: { lang: '' } }}
                                    as={props.as}>
                                    <a>en</a>
                                </Link>
                            </li>
                            <li className={'last'}>
                                <Link href={{ pathname: props.href, query: { lang: 'el' } }}
                                    as={props.as === '/' ? '/el' : `/el${props.as}`}>
                                    <a>gr</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header