import React, { useEffect } from "react";
import NextHead from 'next/head'
import { ENV } from '../env'
import Header from '../components/header'
import Footer from '../components/footer'

const Layout = props => {

  let circleArray = [];
  let colorArray = [
    '115,48,73',
    '215,64,97',
    '89,54,63'
  ]
  
  let frame = 0;

  function Circle(x, y, radius, vx, vy, rgb, opacity, birth, life) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.minRadius = radius;
    this.vx = vx;
    this.vy = vy;
    this.birth = birth;
    this.life = life;
    this.opacity = opacity;

    this.draw = function () {
      const canvas = document.querySelector('canvas');
      const c = canvas.getContext('2d');
      c.beginPath();
      c.arc(this.x, this.y, this.radius, Math.PI * 2, false);
      c.fillStyle = 'rgba(' + rgb + ',' + this.opacity + ')';
      c.fill();
    }

    this.update = function () {
      if (this.x + this.radius > innerWidth || this.x - this.radius < 0) {
        this.vx = -this.vx;
      }

      if (this.y + this.radius > innerHeight || this.y - this.radius < 0) {
        this.vy = -this.vy;
      }

      this.x += this.vx;
      this.y += this.vy;

      this.opacity = 1 - (((frame - this.birth) * 1) / this.life);

      if (frame > this.birth + this.life) {
        for (let i = 0; i < circleArray.length; i++) {
          if (this.birth == circleArray[i].birth && this.life == circleArray[i].life) {
            circleArray.splice(i, 1);
            break;
          }
        }
      } else {
        this.draw();
      }
    }

  }

  const initCanvas = () => {
    circleArray = [];
  }

  const drawCircles = (mouse, c) => {
    for (let i = 0; i < 3; i++) {
      let radius = Math.floor(Math.random() * 4) + 2;
      let vx = (Math.random() * 2) - 1;
      let vy = (Math.random() * 2) - 1;
      let spawnFrame = frame;
      let rgb = colorArray[Math.floor(Math.random() * colorArray.length)];
      let life = 100;
      circleArray.push(new Circle(mouse.x, mouse.y, radius, vx, vy, rgb, 1, spawnFrame, life, c));

    }
  }


  const animate = () => {
    const canvas = document.querySelector('canvas');
    const c = canvas.getContext('2d');
    requestAnimationFrame(animate);
    frame += 1;
    c.clearRect(0, 0, innerWidth, innerHeight);
    for (let i = 0; i < circleArray.length; i++) {
      circleArray[i].update();
    }

  }

  useEffect(() => {
    const canvas = document.querySelector('canvas');
    canvas.height = window.innerHeight;
    canvas.width = window.innerWidth;

    let mouse = {
      x: undefined,
      y: undefined
    }

    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/service-worker.js')
        .then(registration => {
          console.log('Service Worker registration successful')
        })
        .catch(err => {
          console.warn(err.message)
        })
    }
    
    window.addEventListener('resize', function () {
      canvas.height = window.innerHeight;
      canvas.width = window.innerWidth;
      initCanvas();
    })
    window.addEventListener('mousemove',
      function (event) {
        mouse.x = event.x;
        mouse.y = event.y;
        drawCircles(mouse);
      }
    )
    window.addEventListener("touchmove",
      function (event) {
        let touch = event.touches[0];
        mouse.x = touch.clientX;
        mouse.y = touch.clientY;
        drawCircles(mouse);
      }
    )


    initCanvas()
    animate()
  })



  return (
    <React.Fragment>

      <NextHead>
        <meta charSet="UTF-8" />
        <title>{props.seo.title || ''}</title>
        <meta name="description" content={props.seo.description || ''} />
        <meta name="keywords" content={props.seo.keywords || ''} />
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />
        <meta name="theme-color" content="#fff" />

        <link rel="preconnect dns-prefetch" href="https://fonts.gstatic.com" crossOrigin={'true'} />
        <link rel="dns-prefetch" href="//cdnjs.cloudflare.com" crossOrigin={'true'} />
        <link rel="dns-prefetch" href="//ajax.googleapis.com" crossOrigin={'true'} />
        <link rel="dns-prefetch" href="//s.w.org" crossOrigin={'true'} />

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"></link>
        <link href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel="stylesheet"></link>

        <link rel="manifest" href={"/manifest.json"}></link>
        <link rel="shortcut icon" href="/favicon-ibs.ico"></link>
        <link type="text/css" rel="stylesheet" href={"/style.css"}></link>

      </NextHead>

      <Header href={props.header.href} as={props.header.as} lang={props.lang} />
      
      {props.children}

      <Footer />
      <canvas id='canvas'></canvas>
    </React.Fragment>
  )
}
export default Layout
