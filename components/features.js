import React from 'react'
import { ENV } from '../env'



const Features = props => {

    return (
        <div className={'container padding-top-bottom'}>

            <div className={'row justify-content-center'}>
                {props.cards.map((c,i) => {
                    return (
                        <div className={'col-md-6 col-xl-3 '} key={i}>
                            <div className={'text-center card'}>
                            <img src={`/${c.img.src}`} alt={`${c.img.alt}`} />
                            <h2>{c.title}</h2>
                            <p>{c.text}</p>
                            <a href={`${c.link}`} rel={"noopener"} target={"_blank"}>{c.more}</a>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default Features