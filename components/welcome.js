
import { useState, useEffect } from 'react'
import Link from 'next/link'

import { ENV } from '../env'

const Welcome = props => {
    const [state, setState] = useState('')

    useEffect(() => {
        let update = false;

        if (!update) {
            if(window.navigator.userAgent.match(/Firefox\/([0-9]+)\./)){
                setState(window.location.hostname)
            }else{
                (window.location.ancestorOrigins && window.location.ancestorOrigins.length  === 1 )? setState(window.location.ancestorOrigins) : setState(window.location.hostname)
            }  
        }
        return () => {
            update = true;
        };
    })


    return (


        <div className={'container'}>

            <div className={'row align-items-center'}>
                <div className={'col-sm-6  inner-container'}>
                    <h1>{props.title}</h1>
                    <p >{state}</p>
                    <h2>{props.subTitle}</h2>

                    <Link href={{ pathname: '/contact', query: { lang: props.lang } }} as={`/${props.lang === 'el' ? 'el/contact' : 'contact'}`}>
                        <a className={'primary_btn'}>{props.button}</a>
                    </Link>

                </div>
                <div className={'col-sm-6 text-center img'} >
                    <img src={`/assets/homepageImg.png`} alt={'iBS | Informatics Business Services'} />
                </div>
            </div>
        </div>


    )
}
export default Welcome