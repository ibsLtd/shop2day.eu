import React from 'react'


const Modal = props => {

    const close = () => {
        document.querySelector('.modal').classList.remove('active')
    }

    return (


        <div className="modal-content">
            <span className="close" onClick={() => close()}>&times;</span>
            <p>{props.msg.successMsg}</p>
        </div>


    )
}

export default Modal