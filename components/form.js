import React, { useState, useEffect } from 'react'
import axios from 'axios';
import Modal from './modal';




const Form = (props) => {

    const initialState = { name: '', email: '', textarea: '', captcha: '' }
    const [state, setState] = useState({ ...initialState }),
        [errorState, setErrors] = useState({ name: '', email: '' }),
        [domainState, setDomain] = useState('')

    const changeLabel = e => {
        e.target.closest('div').children[0].classList.add('active')
    }
    const handleInput = e => {
        changeLabel(e)
        state[e.target.id] = e.target.value
        setState({ ...state })
    }

    const handleValidation = (fields) => {
        let formIsValid = true;
        let errors = {};

        //Name
        if (!fields["name"]) {
            formIsValid = false;
            errors["name"] = "Cannot be empty";
        }

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Cannot be empty";
        } else {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Email is not valid";
            }
        }

        errorState.name = errors.name
        errorState.email = errors.email
        setErrors({ ...errorState })
        return formIsValid;
    }

    const reset = () => {
        setState({ ...initialState })
    }


    const onSubmit = e => {
        e.preventDefault()
        const valid = handleValidation(state)
        if (valid) {
            const data = {
                domain: domainState,
                name: state.name,
                message: state.textarea,
                _replyto: state.email,
                _subject: 'Alias site form'
            }
            axios.post('https://formspree.io/xyyzdvjr', data, {
                crossDomain: true,
                dataType: "json",
            })
                .then(response => {

                    document.querySelector('.modal').classList.add('.active')
                    reset()

                })
                .catch(err => {
                    console.log(err)
                })

        }

    }


    useEffect(() => {
        let update = false;

        if (!update) {
            setDomain(window.location.hostname)
        }
        return () => {
            update = true;
        };
    })

    return (
        <React.Fragment>
            <form className={props.class}>
                <div className={" d-flex flex-column form-group"}>
                    <input type={"text"} id={"domain"} value={domainState} readOnly />

                </div>
                <div className={" d-flex flex-column form-group"}>
                    <label htmlFor={"name"} >{props.locale.name}<span>*</span></label>
                    <input type={"text"} id={"name"} onClick={changeLabel}
                        onChange={handleInput} />
                    {errorState.name !== '' && <div>{errorState.name}</div>}
                </div>
                <div className="d-flex flex-column form-group">
                    <label htmlFor={"email"}>{props.locale.email}<span>*</span></label>
                    <input type={"email"} id={"email"} onClick={changeLabel}
                        onChange={handleInput} />
                    {errorState.email !== '' && <div>{errorState.email}</div>}
                </div>
                <div className="d-flex flex-column  form-group">
                    <label htmlFor={"textarea"} >{props.locale.mes}</label>
                    <textarea id={"textarea"} onClick={changeLabel} onChange={handleInput} />
                </div>
                <div className={'form-footer d-flex flex-column '}>
                    <div className={'primary_btn'} onClick={onSubmit}>{props.locale.submit}</div>
                </div>

            </form>
            <div className={'modal'}>
                <Modal msg={props.locale} />
            </div>
        </React.Fragment>
    )
}

export default Form