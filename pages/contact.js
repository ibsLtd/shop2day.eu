import React from 'react'
import { ENV } from '../env'
import Layout from '../components/layout'
import Form from '../components/form'
import Map from '../components/map'

const Contact = props => {


    const header = {
        href: props.page.href,
        as: props.page.as
    }





    return (
        <Layout header={header} lang={props.page.lang} seo={props.page.seo}>
            <section className={' section '}>
            <div className={'g_map'}>
                <Map

                    lat={37.9406996}
                    lng={23.6408958}
                    pin={`/assets/icon.png`}
                    googleMapURL={ENV.MAP_API}
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div className={'g_map'} />}
                    mapElement={<div style={{ height: `100%` }} />}
                />
                </div>
            </section>
            <section className={'contact'}>

                <div className={'container'}>
                    <div className={'row'}>
                        <div className={'col-12'}>
                            <h1>{props.page.locale.title}</h1>
                        </div>
                    </div>
                    <div className={'row padding-top-bottom pos-rel'}>
                        <div className={'col-md-7'}>
                            <Form locale={props.page.locale} />
                        </div>
                        <div className={'col-md-5 details text-center'}>
                            <h2>{props.page.locale.details.title}</h2>
                            <div>
                                <p>{props.page.locale.details.subTitle} : <a href={props.page.locale.link} rel={"noopener"} target={"_blank"}>iBS Ltd</a></p>
                                <p className={'fa fa-phone _brand'}>+30 210 4224242</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    )

}




Contact.getInitialProps = async ({ query }) => {
    let page = {}
    page.lang = await query.lang
    page.href = '/contact'
    page.as = '/contact'
    switch (page.lang) {
        case 'el': {

            page.locale = {
                title: 'Επικοινωνία',
                name: 'Ονοματεπώνυμο',
                email: 'Email adress',
                mes: 'Μήνυμα',
                submit: 'Αποστολη',
                'successMsg': 'Eυχαριστουμε που επικοινωνησατε μαζι μας',
                'failMsg': 'Κάτι πήγε στραβά, δοκιμάστε ξανά αργότερα',
                details: {
                    title: "ΠΛΗΡΟΦΟΡΙΕΣ",
                    subTitle: 'Για περισσότερες πληροφορίες, επισκεφθείτε την ιστοσελίδα μας στο',
                    link: 'https://www.ibs.gr/el/'
                }


            }
            page.seo = {
                title: 'Επικοινωνία - iBS | Informatics Business Services',
                description: 'Η iBS προσφέρει συμβουλευτικές υπηρεσίες πληροφορικής σε συνδυασμό με την ανάπτυξη προσαρμοσμένων λύσεων λογισμικού.',
                keywords: 'iBS,Informatics Business Services,software,hosting,support,ibs'

            }
            break;
        }
        default:

            page.locale = {
                title: 'Contact us',
                name: 'Name',
                email: 'Email adress',
                mes: 'Message',
                submit: 'Submit',
                'successMsg': 'Thank you for getting in touch! ',
                'failMsg': 'Something went wrong, try again later',
                details: {
                    title: "CONTACT DETAILS",
                    subTitle: 'For more information, please visit our website at ',
                    link: 'https://www.ibs.gr/'
                }

            }
            page.seo = {
                title: 'Contact us - iBS | Informatics Business Services',
                description: 'iBS’s offering embraces the synergy of information technology consulting services along with custom software development and platform-based customization.',
                keywords: 'iBS,Informatics Business Services,software,hosting,support,ibs'
            }

    }
    return { page }
}
export default Contact
