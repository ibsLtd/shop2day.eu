import React from 'react'
import home from '../public/json/home'
import homeEl from '../public/json/homeEl'

import Layout from '../components/layout'
import Welcome from '../components/welcome'
import Features from '../components/features'

const Index = props => {

  const header = {
    href: props.page.href,
    as: props.page.as
  }


  return (
    <Layout header={header} lang={props.page.lang} seo={props.page.locale.seo}>
      <section className={'section welcome'}>
        <Welcome lang={props.page.lang}
          title={props.page.locale.title}
          subTitle={props.page.locale.subTitle}
          button={props.page.locale.button}
        />

      </section>
      <section className={'features'}>
        <Features cards={props.page.locale.cards} />
      </section>
    </Layout>
  )

}




Index.getInitialProps = async ({ query }) => {
  let page = {}
  page.lang = await query.lang
  page.href = '/'
  page.as = '/'
  switch (page.lang) {
    case 'el': {
      page.locale = { ...homeEl }
      break;
    }
    default:
      page.locale = { ...home }
  }
  return { page }
}
export default Index
